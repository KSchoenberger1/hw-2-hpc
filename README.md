# HW 2 -HPC

(1) convert an existing serial C++ code into a parallel, multi-threaded code using OpenMP 
(2) run a series of benchmarks on Stampede2 to measure the parallel speed-up and efficiency.